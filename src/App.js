import { useState } from "react";
import "./assets/css/App.css";
import botHi from "./assets/images/robot2.jpg";
import botHi2 from "./assets/images/robot3.jpg";
import botX from "./assets/images/robot4.jpg";
import me from "./assets/images/avatar.png";
import FormRegister from "./components/FormRegister";
import Chat from "./components/Chat";

const App = () => {
	const unRegEx = /^[a-zA-Z]{3,}$/;
	const pwRegEx = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
	const chatObj = { who: "bot", image: botHi, message: "Hi, I'm Regbot! What is your username?" };
	const [user, setUser] = useState({ username: "", password: "", confirm: "" });
	const [chats, setChats] = useState([chatObj]);
	const [disable, setDisable] = useState(false);

	function handleSend(chatMessage) {
		setDisable(true);
		setChats([...chats, newChatObj("me", me, chatMessage)]);

		if (!user.username) {
			if (unRegEx.test(chatMessage)) {
				setUser({ ...user, username: chatMessage });
				handleBotChat("bot", botHi, `Great! Now, please enter your password.`);
			} else {
				handleBotChat("bot", botX, "Yikes! That's an invalid username. Must be over 3 letters (only) long.");
			}
		}

		if (user.username && !user.password) {
			if (pwRegEx.test(chatMessage)) {
				setUser({ ...user, password: chatMessage });
				handleBotChat("bot", botHi, "You got it! Your password passed the minimum requirement. Now, please retype your password.");
			} else {
				handleBotChat("bot", botX, "Ooops! The password is weak. It must have at least a caPital letter, a numb3r, and a $pecial charater. Minimum of 8.");
			}
		}

		if (user.username && user.password && !user.confirm) {
			if (chatMessage === user.password) {
				setUser({ ...user, confirm: chatMessage });
				handleBotChat("bot", botHi2, "Woo-hoo! You are now successfully registered. Click the Home button to get in.");
			} else {
				handleBotChat("bot", botX, "Darn it! It didn't match. Confirm your password again.");
			}
		}

		scrollHere(`chat_${chats.length + 1}`);
	}

	function handleBotChat(who, image, message) {
		setTimeout(() => {
			setChats(prevChat => [...prevChat, newChatObj(who, image, message)]);
		}, 1000);
	}

	function newChatObj(who, image, message) {
		return {
			who: who,
			image: image,
			message: message,
		}
	}

	function scrollHere(elemId) {
		setTimeout(() => {
			const targetElem = document.getElementById(elemId);

			targetElem.scrollIntoView({ behavior: 'smooth', block: 'end' });

			setDisable(false);
		}, 1100);
	}

	const showChats = chats.map((chat, index) => <Chat key={index} chat={chat} id={`chat_${index}`} />);

	const showHome = <div className="flex">
		<button>Log Out</button>
		<button className="btn-theme">Home</button>
	</div>

	document.title = "RegBot | By Ed";
	return (
		<main className="container">
			<div className="panel register">
				<div className="panel--header">Register</div>

				<div className="panel--body register--body">
					<div className="chat-container" id="chatBox">{showChats}</div>
				</div>

				<div className="panel--footer register--footer">
					{user.confirm.length ? showHome : <FormRegister send={handleSend} disable={disable} />}
				</div>
			</div>
			<div className="return-container">
				<div className="flex">
					{user.username && <div><small>Username:</small><p>{user.username}</p></div>}
					{user.password && <div><small>Password:</small><p>{user.password}</p></div>}
					{user.confirm && <div><small>Confirm:</small><p>{user.confirm}</p></div>}
				</div>
			</div>
		</main>
	)
}

export default App;