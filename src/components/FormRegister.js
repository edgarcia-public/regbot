import { useState } from "react";

const FormRegister = ({send, disable}) => {
	const [chatMessage, setChatMessage] = useState("");

	function handleSubmit(e) {
		e.preventDefault();

		if (!chatMessage.trim()) return setChatMessage("");

		send(chatMessage.trim());

		setChatMessage("");
	}

	return (
		<form id="regForm" onSubmit={e => handleSubmit(e)}>
			<div className="input-inline">
				<input type="text" value={chatMessage} onChange={e => setChatMessage(e.target.value)} disabled={disable}/>
				<button type="submit" disabled={disable}>Send</button>
			</div>
		</form>
	)
}

export default FormRegister;