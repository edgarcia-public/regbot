const Chat = (props) => {
	return (
		<div className={`chat--item ${props.chat.who}`} id={props.id}>
			{props.chat.who === "bot" ? <img src={props.chat.image} alt="" />:""}
			{props.chat.message ? <div className="chat--box"><p>{props.chat.message}</p></div> : ""}
			{props.chat.who === "me" ? <img src={props.chat.image} alt="" />:""}
		</div>
	)
}

export default Chat;